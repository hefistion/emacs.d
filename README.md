<p align="center"><img src="assets/emacs-logo.png" width=150 height=150/></p>
<p align="center"><a href="https://www.gnu.org/software/emacs/"><b>GNU Emacs</b></a></p>
<p align="center">
	<a href="https://www.gnu.org/software/emacs/"><img src="https://img.shields.io/badge/GNU%20Emacs-26.1-b48ead.svg?style=flat-square"/></a>
	<a href="https://orgmode.org/"><img src="https://img.shields.io/badge/org--mode-9.1.13-489a9f.svg?style=flat-square"/></a>
	<a href="https://github.com/jwiegley/use-package"><img src="https://img.shields.io/badge/use--package-2.4-88c0d0.svg?style=flat-square"/></a>
</p>
<p align="center">Este repositorio contiene toda mi configuración de Emacs GNU.</p>
---

Es una configuración sencilla y orientada a trabajar con [org-mode](https://orgmode.org/)

<p align="center"><img src="assets/emacs-preview.png"/></p>

<blockquote>
    Theme: <a href="https://protesilaos.com/emacs/modus-themes">Modus Themes for GNU Emacs</a>
</blockquote>

Archivos:

* [`Emacs.org`](https://gitlab.com/hefistion/emacs.d/-/blob/master/Emacs.org?ref_type=heads):
  archivo de configuración principal.


## Contribuciones

No soy ningún experto en Lisp, ni en GNU Emacs, estoy seguro que se pueden mejorar
varias funciones e incluso producir errores de ortografía. Si quieres
hacer tu propia corrección en esta configuración, puedes hacerlo en el
[seguimiento de incidencias](https://gitlab.com/hefistion/emacs.d/issues).

## Licencia

El código no tiene licencia, coge lo que quieras, espero que esta configuración
te pueda ser tan útil para ti como lo es para mí.

GNU Emacs es sobre todo un concepto de compartir para facilitar nuestra vida diaria.
