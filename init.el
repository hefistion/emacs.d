;; Emacs 29?
(unless (>= emacs-major-version 29)
  (error "Emacs Writing Studio requires Emacs version 29 or later"))

;; Ajustes personalizados en un archivo separado y cargar la configuración personalizada
(setq-default custom-file
              (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; Establecer archivos de paquetes
(use-package package
  :config
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/"))
  (package-initialize))

;; Gestión de paquetes
(use-package use-package
  :custom
  (use-package-always-ensure t)
  (package-native-compile t)
  (warning-minimum-level :error))

(setq user-full-name "Carlos M.")
(setq inhibit-startup-message t
      use-short-answers t)
(tool-bar-mode -1)                                            ; Desactivar la barra de herramientas
(menu-bar-mode -1)                                            ; Desactivar la barra de menús
(scroll-bar-mode -1)                                          ; Desactivar la barra de desplazamiento visible
(add-to-list 'default-frame-alist '(fullscreen . maximized))  ; Activar frame maximizado
(display-time)                                                ;
(setq display-time-24hr-format t)                             ;
(delete-selection-mode t)                                     ; Sobrescribir el texto seleccionado
(electric-pair-mode 1)                                        ; Cierre automático de bracket.
;;(setq-default line-spacing 2)                             ; Aumentar el interlineado

(set-face-attribute 'default nil :family "Source Code Pro" :height 120 :weight 'light)
(set-face-attribute 'bold nil :weight 'semibold)
(set-face-attribute 'italic nil :family "Source Code Pro")

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

(setq modus-themes-bold-constructs t
      modus-themes-italic-constructs t
      modus-themes-fringes 'subtle
      modus-themes-tabs-accented t
      modus-themes-paren-match '(bold intense)
      modus-themes-prompts '(bold intense)
      modus-themes-completions 'opinionated
      modus-themes-org-blocks 'gray-background
      modus-themes-scale-headings t
      modus-themes-to-toggle
      '(modus-operand modus-vivendi)
      modus-themes-region '(bg-only)
      modus-themes-headings
      '((1 . (rainbow overline background 1.4))
        (2 . (rainbow background 1.3))
        (3 . (rainbow bold 1.2))
        (t . (semilight 1.1))))

;; Inicia en el tema oscuro por defecto
(load-theme 'modus-vivendi)             
;; F5 para cambiar entre modus-operandi y modus-vivendi
(define-key global-map (kbd "<f5>") #'modus-themes-toggle)

(use-package vertico
  :init
  (vertico-mode)
  :custom
  (vertico-count 13)                    ; Número de candidatos a mostrar
  (vertico-resize t)
  (vertico-cycle t)
  (vertico-sort-function 'vertico-sort-history-alpha))

(use-package savehist
  :init
  (savehist-mode))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides
   '((file (styles partial-completion)))))

(use-package marginalia
  :init
  (marginalia-mode))

(use-package consult
  :bind (
         ("C-c M-x" . consult-mode-command)
         ;; ("C-c k" . consult-kmacro)
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("M-g o" . consult-outline)               ;; Alternativa: consult-org-heading
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ("M-s d" . consult-find)                  ;; Alternativa: consult-fd
         ("M-s g" . consult-grep)
         ("M-s l" . consult-line)))

(setq auth-source-debug  t)
(setq auth-sources
      '((:source "~/.gnupg/shared/authinfo.gpg")))

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-delete-old-versions t)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

(use-package flyspell
  :config
  (setq ispell-program-name "hunspell"
        ispell-default-dictionary "es_ES")
  :hook (text-mode . flyspell-mode)
  :bind (("M-<f7>" . flyspell-buffer)
         ("<f7>" . flyspell-word)))

(use-package flyspell-correct
  :after (flyspell)
  :bind (("C-;" . flyspell-auto-correct-previous-word)
         ("<f7>" . flyspell-correct-wrapper)))

(use-package undo-tree
  :init
  (global-undo-tree-mode 1)
  :custom
  (undo-tree-visualizer-timestamps t)
  (undo-tree-visualizer-diff t)
  (undo-tree-auto-save-history nil))

(use-package autorevert
  :ensure nil
  :diminish
  :hook (after-init . global-auto-revert-mode))

(use-package recentf
  :defer 2
  :bind ("C-c r" . recentf-open-files)
  :init (recentf-mode)
  :custom
  (recentf-max-menu-items 10)
  (recentf-max-saved-items 50)
  (recentf-exclude (list "COMMIT_EDITMSG"
                         "~$"
                         "/scp:"
                         "/ssh:"
                         "/sudo:"
                         "diario.*"
                         "recentf*"
                         "bookmark*"
                         "/archivo*"
                         "birthday*"
                         "*elpa/*"
                         "/tmp/"
                         "drafts/*"
                         "/.elfeed"
                         "/.config"
                         "~/.emacs.d/url/*"
                         "~/.emacs.d/s*"))
  :config (run-at-time nil (* 5 60) 'recentf-save-list))

(global-set-key [remap kill-buffer] #'kill-this-buffer)
;;(kill-buffer "*scratch*")

(use-package all-the-icons)

(setq dashboard-icon-type 'all-the-icons)
(use-package dashboard
  :custom
  (dashboard-startup-banner "~/Imágenes/mariposa.png")
  (dashboard-banner-logo-title (format "Buen día %s" user-full-name))
  (dashboard-items '((recents . 4)
                     (bookmarks . 4)
                     (agenda . 3)))
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-set-heading-icons t
        dashboard-set-file-icons t
        dashboard-set-init-info t
        dashboard-set-navigator t)
  (setq dashboard-navigator-buttons
        `((
           (,(when (display-graphic-p)
               (all-the-icons-octicon "home" :height 1.1 :v-adjust 0.0))
            "Página web" "El Blog de Lázaro"
            (lambda (&rest _) (browse-url "https://elblogdelazaro.org")))
           (,(when (display-graphic-p)
               (all-the-icons-material "home" :height 1.35 :v-adjust -0.24))
            "Localhost" "Abrir Hugo localhost"
            (lambda (&rest _) (browse-url "http://localhost:1313/")))
           (,(when (display-graphic-p)
               (all-the-icons-octicon "tools" :height 1.0 :v-adjust 0.0))
            "Configuración" "Abrir configuración de emacs (.org)"
             (lambda (&rest _) (find-file (expand-file-name  "~/.emacs.d/Emacs.org"))))
           (,(when (display-graphic-p)
               (all-the-icons-octicon "calendar" :height 1.0 :v-adjust 0.0))
            "Agenda" "Agenda personal"
            (lambda (&rest _)
              (interactive)
              (if (get-buffer "*Org Agenda*")
                  (progn
                    (switch-to-buffer-other-window "*Org Agenda*")
                    (kill-buffer "*Org Agenda*")
                    (org-agenda-list))
                (split-window-right)
                (org-agenda-list))))
           ))))
;; F10 para ir al Dashboard
(global-set-key (kbd "<f10>") 'open-dashboard)

(defun open-dashboard ()
  "Abre el buffer *dashboard* y salta al primer widget."
  (interactive)
  (delete-other-windows)
  ;; Refresca  dashboard buffer
  (if (get-buffer dashboard-buffer-name)
      (kill-buffer dashboard-buffer-name))
  (dashboard-insert-startupify-lists)
  (switch-to-buffer dashboard-buffer-name))

(use-package which-key
  :config
  (which-key-mode))

(use-package nov
  :init
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))
(use-package doc-view
  :custom
  (doc-view-resolution 300)
  (doc-view-mupdf-use-svg t)
  (large-file-warning-threshold (* 50 (expt 2 20))))

(use-package doc-view
  :custom
  (doc-view-resolution 300)
  (doc-view-mupdf-use-svg t)
  (large-file-warning-threshold (* 50 (expt 2 20))))

(defun ews-distraction-free ()
    "Distraction-free writing environment using Olivetti package."
    (interactive)
    (if (equal olivetti-mode nil)
        (progn
          (window-configuration-to-register 1)
          (delete-other-windows)
          (text-scale-set 2)
          (olivetti-mode t))
      (progn
        (if (eq (length (window-list)) 1)
            (jump-to-register 1))
        (olivetti-mode 0)
        (text-scale-set 0))))

  (use-package olivetti
    :demand t
    :bind
    (("<f9>" . ews-distraction-free)))

(setq initial-major-mode 'org-mode
      initial-scratch-message
      "#+title: Scratch Buffer\n\nFor random thoughts.\n\n")

(use-package persistent-scratch
  :hook
  (after-init . persistent-scratch-setup-default)
  :init
  (persistent-scratch-setup-default)
  (persistent-scratch-autosave-mode)
  :bind
  (("C-c w x" . scratch-buffer)))

(use-package company
  :custom
  (company-idle-delay 0)
  (company-minimum-prefix-length 4)
  (company-selection-wrap-around t)
  :init
  (global-company-mode))

(use-package company-posframe
  :config
  (company-posframe-mode 1))

(use-package dired
  :ensure
  nil
  :commands
  (dired dired-jump)
  :custom
  (dired-listing-switches
   "-goah --group-directories-first --time-style=long-iso")
  (dired-dwim-target t)
  (delete-by-moving-to-trash t)
  :init  ;; Abrir carpetas dired en el mismo búfer
  (put 'dired-find-alternate-file 'disabled nil))

;; Ocultar archivos ocultos
(use-package dired-hide-dotfiles
  :hook
  (dired-mode . dired-hide-dotfiles-mode)
  :bind
  (:map dired-mode-map ("." . dired-hide-dotfiles-mode)))

;; Archivos de copia de seguridad
(setq-default backup-directory-alist
              `(("." . ,(expand-file-name "backups/" user-emacs-directory)))
              create-lockfiles nil)  ; No crea ficheros lock

;; Visor de imágenes
(use-package emacs
  :bind
  ((:map image-mode-map
              ("k" . image-kill-buffer)
              ("<right>" . image-next-file)
              ("<left>"  . image-previous-file))
   (:map dired-mode-map
    ("C-<return>" . image-dired-dired-display-external))))

(setq-default org-startup-indented t
                org-pretty-entities t
                org-use-sub-superscripts "{}"
                org-hide-emphasis-markers t
                org-startup-with-inline-images t
                org-image-actual-width '(300))

;; Mostrar marcadores de énfasis ocultos
(use-package org-appear
    :hook
    (org-mode . org-appear-mode))

(use-package org
  :config
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  ;; archivos utilizados por la agenda
  (setq org-agenda-files
        '("~/.personal/agenda/personal.org"
          "~/.personal/agenda/trabajo.org"
          "~/.personal/agenda/diario-familia.org"
          "~/.personal/agenda/diario-familia-ibx.org"
          "~/.personal/agenda/diario-personal.org"
          "~/.personal/agenda/diario-personal-ibx.org"))

  ;; utilizo mi propio diario para a la agenda, asi que deshabilito el de emacs
  (setq org-agenda-include-diary nil)
  (setq org-agenda-diary-file "~/.personal/agenda/diario-familia.org")

  ;; ubicacion de los ficheros cuando son archivados, organizados por fecha
  (setq org-archive-location "~/.personal/archivo/%s_archivo.org::datetree/")

  (setq org-todo-keywords
              '((sequence "PORHACER(p!)"
                          "ENPROCESO(e!)"
                          "BLOQUEADO(b!)"
                          "|" "HECHO(h!)" "ARCHIVAR(a!)")))

  ;; (setq org-todo-keyword-faces
  ;;       '(("PORHACER" . "#ff0000")
  ;;         ("ENPROCESO" . "#ff00ff")
  ;;         ("BLOQUEADO" . "#ff00ff")
  ;;         ("HECHO" . "#a3cfa0")
  ;;         ("ARCHIVAR" . "#ffff00")))

  (setq org-refile-targets
        '(("personal.org" :maxlevel . 1)
          ("trabajo.org" :maxlevel . 1)))

  ;; Guarda los buffers Org despues de rellenar
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  ;; Etiquetas que utilizo para mis notas
  (setq org-tag-alist '(("@nota" . ?n)
                        ("@casa" . ?c)
                        ("@finanzas" . ?d)
                        ("@fecha" . ?f)
                        ("@salud" . ?s)
                        ("@tarea" . ?t)
                        ("@coche" . ?h)
                        ("@trabajo" . ?b)
                        ("crypt" . ?C)))
  (setq org-tags-exclude-from-inheritance '("crypt"))

  ;; Registro del progresodelas tareas
  ;; Cuando un elemento TODO entra en DONE, añade una propiedad CLOSED: con la fecha y hora actual en el cajón
  (setq org-log-done 'time)
  (setq org-log-into-drawer "LOGBOOK")

  ;; Alinea etiquetas
  (setq org-tags-column 70))

;; Aspecto mejorado al identar
(add-hook 'org-mode-hook 'org-indent-mode)

;; Finalmente haremos que cuando se visualice un fichero con extensión .org éste se adapte a la ventana y cuando la línea llegue al final de esta, haga un salto de carro.
(add-hook 'org-mode-hook 'visual-line-mode)

;; Actualiza los ficheros Org con la última fecha de modificación cuando #+lastmod: está disponible
(setq time-stamp-active t
      time-stamp-start "#\\+lastmod:[ \t]*"
      time-stamp-end "$"
      time-stamp-format "[%04Y-%02m-%02d %a]")
(add-hook 'before-save-hook 'time-stamp nil)

(use-package org-contacts
  :after org
  :custom
  (org-contacts-files '("~/.personal/agenda/contactos.org")))

(global-set-key (kbd "<f6>") 'org-agenda)

    (use-package org-super-agenda
      :config
      (org-super-agenda-mode))

    (setq org-agenda-skip-scheduled-if-done t
          org-agenda-skip-deadline-if-done t
          org-agenda-compact-blocks t
          org-agenda-window-setup 'current-window
          org-agenda-start-on-weekday 1
          org-deadline-warning-days 7
          org-agenda-time-grid '((daily today require-timed))
          org-agenda-custom-commands
          '(
            ("x" "Vista trabajo"
             ((agenda "" ((org-agenda-span 3)
                          (org-super-agenda-groups
                           '((:name "Hoy"
                                    :discard (:tag "personal")
                                    :time-grid t
                                    :scheduled past
                                    :deadline past
                                    :date today
                                    :order 1)))))
              (alltodo "" ((org-agenda-overriding-header "")
                           (org-super-agenda-groups
                            '((:discard (:tag "personal" ))
                              (:name "Vencimiento hoy"
                                     :deadline today
                                     :order 5)
                              (:name "Próximamente"
                                     :deadline future
                                     :order 11)
                              (:name "Atrasado"
                                     :scheduled past
                                     :deadline past
                                     :order 12)
                              (:name "Por hacer"
                                      ;:discard (:scheduled future :deadline future)
                                     :todo "PORHACER"
                                     :order 12)
                              (:name "Esperando"
                                     :todo "BLOQUEADO"
                                     :order 14)))))
              (tags "trabajo/HECHO"
                    ((org-agenda-overriding-header " Tareas Hechas")))))

            ("z" "Vista personal"
             ((agenda "" ((org-agenda-span 3)
                          (org-super-agenda-groups
                           '((:name "Hoy"
                                    :discard (:tag "trabajo" :scheduled past :deadline past)
                                    :time-grid t
                                    :date today
                                    :scheduled today
                                    :order 1)
                             (:name ""
                                    :tag "agenda"
                                    :todo "Aniversarios")))))
              (alltodo "" ((org-agenda-overriding-header "")
                           (org-super-agenda-groups
                            '((:discard (:tag "trabajo" ))
                              (:name "Vencimiento hoy"
                                     :deadline today
                                     :order 5)
                              (:name "Atrasado"
                                     :scheduled past
                                     :deadline past
                                     :order 11)
                              (:name "Por hacer"
                                     :discard (:scheduled future :deadline future)
                                     :todo "PORHACER"
                                     :order 12)
                              (:name "Esperando"
                                     :todo "BLOQUEADO"
                                     :order 14)))))
              (tags "personal/HECHO"
                    ((org-agenda-overriding-header " Tareas Hechas")))))
            ))

(require 'appt)
(appt-activate 1)

(use-package notifications
  :demand t)
(add-hook 'org-finalize-agenda-hook 'org-agenda-to-appt) ;; actualiza la lista de  citas en la vista agenda 

(setq appt-display-format 'window
      appt-message-warning-time '5)
(setq appt-disp-window-function
      (lambda (nmins curtime msg)
        (notifications-notify :title "Recordatorio!!"
                              :body (format "Tienes una cita %s en %d minutos" msg (string-to-number nmins))
                              :app-name "Emacs: Org"
                              :sound-name "alarm-clock-elapsed")))
(display-time)   ;; activar la visualización de la hora
(run-at-time "24:01" 3600 'org-agenda-to-appt)   ;; actualizar la lista de citas cada hora
(setq org-agenda-finalize-hook 'org-agenda-to-appt)

(use-package org
  :bind ("C-c c" . org-capture)
  :preface
  (defvar org-basic-task-template "* PORHACER %?
   Añadido: %U" "Plantilla básica de tareas.")

  (defvar org-meeting-template "* Cita con %^{CON}
   :PROPERTIES:
   :SUMMARY: %^{DESCRIPCION ....}
   :NOMOBRE: %^{NOMBRE ....}
   :LUGAR: %^{DONDE ....}
   :DIRECCION: %^{CALLE ....}
   :TELEFONO: %^{123-456-789}
   :NOTA: %^{NOTAS}
   :AÑADIDA: %U
   :END:
   Fecha de la reunión: %?%T" "Plantilla para programar reuniones.")

  (defvar org-contacts-template "* %(org-contacts-template-name)
   :PROPERTIES:
   :EMAIL: %(org-contacts-template-email)
   :PHONE: %^{123-456-789}
   :HOUSE: %^{123-456-789}
   :ALIAS: %^{nick}
   :NICKNAME: %^{hefistion}
   :IGNORE:
   :NOTE: %^{NOTA}
   :ADDRESS: %^{Calle Ejemplo 1 2A, 28320, Pinto, Madrid, España}
   :BIRTHDAY: %^{yyyy-mm-dd}
   :END:" "Plantilla para org-contacts.")
  :custom
  (org-capture-templates
   `(("c" "Contactos" entry (file+headline "~/.personal/agenda/contactos.org" "Amigos"),
      org-contacts-template)

     ("n" "Nota" entry (file+headline "~/.personal/agenda/reubicar.org" "Nota"),
      org-basic-task-template)

     ("i" "Cita familiar" entry (file+datetree "~/.personal/agenda/diario-familia.org"),
      org-meeting-template)

     ("I" "Cita personal" entry (file+datetree "~/.personal/agenda/diario-personal.org"),
      org-meeting-template)

     ("t" "Tarea" entry (file+headline "~/.personal/agenda/reubicar.org" "Tareas"),
      org-basic-task-template))))

(use-package org
    :after org
    :config
    (require 'org-crypt))
    (org-crypt-use-before-save-magic)
    (setq org-tags-exclude-from-inheritance (quote ("crypt")))
    (setq org-crypt-key "carlosxx@xxxxx.xx")
    (setq org-crypt-disable-auto-save nil)

(setq org-use-speed-commands 'my-org-use-speed-commands-for-headings-and-lists)
(with-eval-after-load 'org
  (let ((listvar (if (boundp 'org-speed-commands) 'org-speed-commands
                   'org-speed-commands-user)))
    (add-to-list listvar '("A" org-archive-subtree-default))
    (add-to-list listvar '("a" org-todo "ARCHIVAR"))
    (add-to-list listvar '("b" org-todo "BLOQUEADO"))
    (add-to-list listvar '("e" org-todo "ENPROCESO"))
    (add-to-list listvar '("x" org-todo "HECHO"))
    (add-to-list listvar '("x" org-todo-yesterday "HECHO"))
    (add-to-list listvar '("s" call-interactively 'org-schedule))
    (add-to-list listvar '("i" call-interactively 'org-clock-in))
    (add-to-list listvar '("o" call-interactively 'org-clock-out))
    (add-to-list listvar '("d" call-interactively 'org-clock-display))
    (add-to-list listvar '("$" call-interactively 'org-archive-subtree))))

;; Cambiamos el estado de la tarea cuando inicia o para el reloj
(setq org-clock-in-switch-to-state "ENPROCESO")
(setq org-clock-out-switch-to-state "BLOQUEADO")

;; Funciones propias
    (defun my-org-use-speed-commands-for-headings-and-lists ()
        "Activate speed commands on list items too."
      (or (and (looking-at org-outline-regexp) (looking-back "^\**" nil))
          (save-excursion (and (looking-at (org-item-re)) (looking-back "^[ \t]*" nil)))))

(setq calendar-month-name-array
      ["Enero" "Febrero" "Marzo" "Abril" "Mayo" "Junio"
       "Julio"    "Agosto"   "Septiembre" "Octubre" "Noviembre" "Diciembre"])

(setq calendar-day-name-array
      ["Domingo" "Lunes" "Martes" "Miércoles" "Jueves" "Viernes" "Sábado"])

(setq org-icalendar-timezone "Europe/Madrid") ;; timezone
(setq calendar-week-start-day 1) ;; la semana empieza el lunes
(setq european-calendar-style t) ;; estilo europeo

(setq calendar-holidays '((holiday-fixed 1 1 "Año Nuevo")
                          (holiday-fixed 1 6 "Reyes Magos")
                          (holiday-fixed 3 19 "San José")
                          (holiday-fixed 5 1 "Dia del Trabajo")
                          (holiday-fixed 5 2 "Comunidad de Madrid")
                          (holiday-fixed 5 15 "San Isidro")
                          (holiday-fixed 8 15 "Asunción")
                          (holiday-fixed 10 02 "Día de la Policía")
                          (holiday-fixed 10 12 "Día de la Hispanidad")
                          (holiday-fixed 11 01 "Todos los Santos")
                          (holiday-fixed 11 09 "Almudena")
                          (holiday-fixed 12 06 "Constitución")
                          (holiday-fixed 12 08 "Inmaculada")
                          (holiday-fixed 12 25 "Navidad")
                          ))

(use-package calfw
  :config
  (setq cfw:org-overwrite-default-keybinding t)) ;; atajos de teclado de la agenda org-mode
;; (setq cfw:display-calendar-holidays nil) ;; para esconder fiestas calendario emacs

(use-package calfw-org
  :ensure t
  :config
  (setq cfw:org-overwrite-default-keybinding t)
  :bind ([f8] . cfw:open-org-calendar))

(use-package org-caldav
  :bind ([f4] . org-caldav-sync)
  :config
  ;; Calendarios a utilizar
  (setq org-caldav-url "https://XXX.XXXXXXXXXXX.XX")
  (setq org-caldav-calendars
        '((:calendar-id "familia/XXXXXXXXXXXXXXXXXXXXX"
                        :files ("~/.personal/agenda/diario-familia.org")
                        :inbox "~/.personal/agenda/diario-familia-ibx.org")
          (:calendar-id "familia/XXXXXXXXXXXXXXXXXXXXXX"
                        :files ("~/.personal/agenda/diario-personal.org")
                        :inbox "~/.personal/agenda/diario-personal-ibx.org"
                        )))
  (setq org-caldav-backup-file "~/.personal/calendario/org-caldav-backup.org")
  (setq org-caldav-save-directory "~/.personal/calendario/")
  (setq org-icalendar-alarm-time 1))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((lisp . t)
   (python . t)
   (shell . t)
   ))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
;; (add-to-list 'org-structure-template-alist '("d" . "description"))
;; (add-to-list 'org-structure-template-alist '("n" . "note"))
;; (add-to-list 'org-structure-template-alist '("al" . "alert"))

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown2")
  :config
  (setq visual-line-column 80)
  (setq markdown-fontify-code-blocks-natively t)
  (setq markdown-enable-math t))

(use-package magit
  :bind
  ("C-x g" . magit-status))

(use-package git-gutter
  :defer 0.3
  :delight
  :init (global-git-gutter-mode))

(use-package git-timemachine
  :defer 1
  :delight)

(use-package ox-hugo
  :after ox)

(use-package vterm
  :bind
  (:map
   vterm-mode-map
   ("C-y" . vterm-yank)
   ("C-q" . vterm-send-next-key)))

(custom-set-variables
 '(tramp-default-method "ssh")
 '(tramp-default-user "root")
 '(tramp-default-host "192.168.1.2#2231"))

(use-package elfeed-goodies
  :after elfeed
  :config
  (elfeed-goodies/setup)
  (setq elfeed-goodies/entry-pane-position 'bottom
        elfeed-goodies/powerline-default-separator 'bar
        elfeed-goodies/entry-pane-size 0.65
        elfeed-goodies/feed-source-column-width 13
        elfeed-goodies/tag-column-width 24))
;; elfeed-goodies/switch-to-entry nil))

(use-package elfeed
  :ensure t
  :bind
  ("C-x w" . elfeed)
  :config
  (setq elfeed-use-curl t))

(use-package elfeed-protocol
  :demand t
  :after elfeed
  :bind
  ("C-x q" . elfeed-protocol-fever-reinit)
  :config
  (elfeed-protocol-enable)
  (defun elfeed-search-format-date (date)
    (format-time-string "%Y-%m-%d %H:%M" (seconds-to-time date)))
  :custom
  (elfeed-use-curl t)
  ;; (elfeed-protocol-fever-maxsize 100)
  (elfeed-log-level 'debug)
  (elfeed-protocol-fever-update-unread-only t)
  (elfeed-protocol-feeds (list
                          (list "fever+https://XXXXX@XXXX.XX"
                                :api-url "https://XXXXX@XXXX.XX/fever/"
                                :password "XXXXXXXXXXXX"
                                ;;:autotags '(("example.com" comic))
                                ))))

(use-package mastodon)
(setq mastodon-instance-url "https://mastodon.social"
      mastodon-active-user "@elblogdelazaro@mastodon.social")
(setq mastodon-tl--highlight-current-toot 1)

(use-package lingva
  :config
  (setq lingva-source "auto"
        lingva-target "es"
        lingva-instance "https://translate.plausibility.cloud"))
